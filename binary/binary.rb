
class Binary
  VERSION = 1

  def initialize(string)
    @binary = validate(string)
  end

  def to_decimal
    @binary.length.times.reduce(0) do |decimal, index|
      decimal + decimal_value(@binary[index], index)
    end
  end

  private
  def validate(string)
    string =~ /\A[01]+\z/ ? string : non_binary_error(string)
  end

  def decimal_value(digit, index)
    digit.eql?('0') ? 0 : 2 ** (@binary.length - index.next)
  end

  def non_binary_error(string)
    raise ArgumentError, "#{string} is not a valid binary."
  end
end
