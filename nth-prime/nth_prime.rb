
class Prime
  @@primes = [3]
  @@number = 6

  def nth(member)
    member_error if member < 1
    return 2 if member.eql? 1

    while @@primes.length < member.pred do
      @@primes << @@number - 1 if is_prime? @@number - 1
      @@primes << @@number + 1 if is_prime? @@number + 1
      @@number += 6
    end

    @@primes[member - 2]
  end

  private
  def is_prime?(number)
    limit = Math.sqrt(number)

    @@primes.each do |prime|
      break if prime > limit
      return false if (number % prime).eql? 0
    end

    true
  end

  def member_error
    raise ArgumentError, 'The provided member place should be >= 1'
  end
end
