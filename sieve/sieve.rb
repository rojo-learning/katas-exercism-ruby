
class Sieve
  def initialize(limit)
    @limit = limit
    @set   = [3] + possible_primes
  end

  def primes
    @primes ||= @limit.eql?(2) ? [2] : [2] + sieve
  end

  private
  def possible_primes
    [].tap { |p| (6..@limit).step(6) { |n| p << n.pred; p << n.next } }
  end

  def sieve
    @set.clone.tap do |possible|
      0.upto(possible.length.pred) do |index|
        next unless tester = possible[index]

        index.next.upto(possible.length.pred) do |i|
          next if possible[i].nil?
          possible[i] = nil if possible[i] % tester == 0
        end
      end

      possible.compact!
    end
  end
end
