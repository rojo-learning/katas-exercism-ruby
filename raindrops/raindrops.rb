
class Raindrops
  DROPS = {
    3 => 'Pling',
    5 => 'Plang',
    7 => 'Plong'
  }

  def self.convert(number)
    ''.tap do |rain|
      DROPS.each { |prime, drop| rain << drop if number % prime == 0 }
      rain << number.to_s if rain.empty?
    end
  end
end
