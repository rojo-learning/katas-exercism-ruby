
class Series
  def initialize(digits)
    @digits = digits.chars.map(&:to_i)
  end

  def slices(slice_size)
    slice_size_error if slice_size > @digits.size

    [].tap do |series|
      0.upto(@digits.length - slice_size) do |start|
        series.push @digits.slice(start, slice_size)
      end
    end
  end

  private
    def slice_size_error
      raise ArgumentError,
        "The requested slize size is bigger than the digits size."
    end
end
