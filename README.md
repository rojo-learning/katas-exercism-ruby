# Exercism - Ruby #

This repository contains solutions in Ruby to the proposed programming practices
on the [Exercism.io](http://exercism.io/) community, following the TDD approach.

You can see the provided solutions with the included comments on
[my Exercism profile](http://exercism.io/Rojo). (Account required).

## Contents ##

- **Accumulate**: Manual implementation of a function that performs an
  operation to every element of a collection and returns a new collection with
  the results.
- **Acronym**: Convert a long phrase to its acronym.
- **All Your Base**: Converts a number, epresented as a sequence of digits in
  one base, to any other base.
- **Binary**: Binary to decimal conversion from a string.
- **Bob**: Selects the right answer a fictional character would return depending
   on the given phrase.
- **Difference of Squares**: Finds the difference between the sum of the squares
  and the square of the sums of the first N natural numbers.
- **Food Chain**: Outputs N verses the lyrics from the cumulative song 'I Know
  an Old Lady Who Swallowed a Fly'.
- **Gigasecond**: Adds one billion seconds to a given date.
- **Grade School**: Small in-memory archiving program that stores students'
  names along with the grade that they are in.
- **Grains**: Calculates the number of grains of wheat on a chessboard given
  that the number on each square doubles.
- **Hamming**: Calculates the Hamming distance between two DNA strands.
- **Hello World**: A simple program to greet the user.
- **Leap**: Will take a year and report if it is a leap year.
- **Nth Prime**: Program that can tell you what the nth prime is.
- **Phone Number**: Cleans up user-entered phone numbers so that they can be
  sent SMS messages.
- **Prime Factors**: Compute the prime factors of a given natural number.
- **RNA Transcription**: Provides the RNA complement of the given DNA strand.
- **Rain Drops**: Converts a number to a string depending of the number prime
  factors. (An advanced version of FizzBuzz).
- **Robot Name**: Program that manages robot name factory settings.
- **Roman Numerals**: Converts from Arabic numbers to Roman numerals.
- **Series**: Program that will take a string of digits and give you all the
  possible consecutive number series of length `n` in that string.
- **Sieve**: Program that uses the Sieve of Eratosthenes to find all the primes
  from 2 up to a given number.
- **Word Count**: Given a phrase can count the occurrences of each word in that
  phrase.

---
Program solutions to the exercises should be considered _public domain_. The
unit test code from Exercism is included only as reference under _fair use_.
