
module RomanNumerals
  def to_roman
    left     = self
    numerals = {
      1000 =>  'M', 900 => 'CM', 500 =>  'D', 400 => 'CD', 100 =>  'C',
      90   => 'XC', 50  =>  'L', 40  => 'XL', 10  =>  'X', 9   => 'IX',
      5    =>  'V', 4   => 'IV', 1   =>  'I'
    }

    numerals.each_with_object('') do | (amount, numeral), roman |
      roman << numeral * (left  / amount)
      left  %= amount
    end
  end
end

class Integer
  prepend RomanNumerals
end
