
class Grains
  def self.square(n)
    2 ** n.pred
  end

  def self.total
    square(65).pred
  end
end
