
class Complement
  MAPPINGS = {
    'A' => 'U',
    'T' => 'A',
    'C' => 'G',
    'G' => 'C'
  }

  def self.of_dna(strand)
    strand.chars.
      map { |n| MAPPINGS[n] or raise complement_error(n, 'DNA') }.join
  end

  def self.of_rna(strand)
    strand.chars.
      map { |n| MAPPINGS.key(n) or complement_error(n, 'RNA') }.join
  end

  private
  def self.complement_error(nucleotide, type)
    raise ArgumentError,
      "The #{nucleotide} base doesn't belong to a #{type} secuence."
  end
end
