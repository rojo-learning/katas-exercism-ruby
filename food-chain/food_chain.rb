
class FoodChain
  VERSION = 1

  VERSES  = {
    fly: "I don't know why she swallowed the fly. Perhaps she'll die.",
    spider: "It wriggled and jiggled and tickled inside her.",
    bird: "How absurd to swallow a bird!",
    cat: "Imagine that, to swallow a cat!",
    dog: "What a hog, to swallow a dog!",
    goat: "Just opened her throat and swallowed a goat!",
    cow: "I don't know how she swallowed a cow!",
    horse: "She's dead, of course!"
  }

  SPECIAL = {
    spider: VERSES[:spider].sub('It', ' that')
  }

  ANIMALS = VERSES.keys

  def self.song
    chase = ''

    ANIMALS.each_with_object('') do |animal, song|
      if animal.eql? ANIMALS.first or animal.eql? ANIMALS.last
        song << head_verse(animal)
      else
        chase.prepend catch_line(animal)
        song << strophe(animal, chase)
      end

      song << "\n" unless animal.eql? ANIMALS.last
    end
  end

  def self.strophe(animal, chase)
    head_verse(animal).tap do |text|
      text << chase
      text << VERSES[ANIMALS.first] << "\n"
    end
  end

  def self.head_verse(animal)
    "I know an old lady who swallowed a #{animal}.\n" <<
      VERSES[animal] << "\n"
  end

  def self.catch_line(animal)
    prey = ANIMALS[ANIMALS.index(animal).pred]
    "She swallowed the #{animal} to catch the #{prey}.".tap do |text|
      text.chomp!('.') << SPECIAL[prey] if SPECIAL[prey]
      text << "\n"
    end
  end
end
