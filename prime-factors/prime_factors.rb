
require 'prime'

class PrimeFactors

  def self.for(number)
    [].tap do |factors|
      while number != 1
        factors << Prime.find { |prime| number % prime == 0 }
        number /= factors.last
      end
    end
  end

end
