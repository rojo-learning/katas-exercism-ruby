
module Accumulate
  def accumulate
    each_with_object([]) { |elem, arry| arry << yield(elem) }
  end
end

Array.include Accumulate
