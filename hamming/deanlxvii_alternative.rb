# If you add an instance method to the module, this module can be used as an
# mixin
module Hamming
  def hamming(other)
    self.compute(self, other)
  end

  extend self

  def compute(s, t)
    [s.size, t.size].min.times.count do |i|
      s[i] != t[i]
    end
  end
end

# and the module can be mixedin
require './hamming'

class String
  include Hamming
end

class Array
  include Hamming
end

puts 'AACCTT'.hamming('ACTTT')

puts ['A','A'].hamming(['A','B'])
