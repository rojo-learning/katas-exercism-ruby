
class PhoneNumber
  def initialize(phone)
    @phone = validate(sanitize phone)
  end

  def number
    @phone
  end

  def area_code
    @phone[0,3]
  end

  def to_s
    "(#{area_code}) #{@phone[3,3]}-#{@phone[6,4]}"
  end

  private
    def sanitize(phone)
      phone.gsub /[^0-9a-z]/i, ''
    end

    def validate(digits)
      if digits.match /[0-9]{10,12}/
        case digits.size
        when 10 then return digits
        when 11 then return digits[1,10] if digits.start_with?('1')
        else
          return digits[2,10] unless digits.start_with?('1')
        end
      end

      '0' * 10
    end
end
