
class Robot
  @@names  = []
  ALPHABET = ('A'..'Z').to_a
  LETTERS  =  2 # Number of letters in the name.
  NUMBERS  =  3 # Number of digits in the name.
  CHANCES  = 10 # Number of tries to generate an unique name.

  attr_reader :name

  def initialize
    reset
  end

  def reset
    CHANCES.times do
      name = random_name
      if @@names.include? name
        continue
      else
        register name and return
      end
    end

    name_error
  end

  private
  def random_name
    stick(LETTERS) { ALPHABET.sample } << stick(NUMBERS) { rand(10).to_s }
  end

  def stick(n)
    ''.tap { |s| n.times { s << yield } }
  end

  def register(name)
    @name    = name
    @@names << name
  end

  def name_error
    raise 'Unable to generate an unique name for the robot.'
  end
end
