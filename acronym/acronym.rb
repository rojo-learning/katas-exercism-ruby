
class Acronym

  COMPONENTS = /\b\w|(?<=[a-z])[A-Z]/

  def self.abbreviate(phrase)
    phrase.scan(COMPONENTS).join.upcase
  end

end

module BookKeeping
  VERSION = 2
end
