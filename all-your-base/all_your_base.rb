
module BaseConverter
  def self.convert(input_base, digits, output_base)
    validate_base input_base
    validate_base output_base
    validate_digits digits, input_base
    return digits if digits.empty?
    return [0] if digits.sum.eql? 0
    decimal_to_base base_to_decimal(input_base, digits), output_base
  end

  def self.base_to_decimal(base, digits)
    (0...digits.size).map { |i| digits[i] * base ** (digits.size.pred - i) }.sum
  end

  def self.decimal_to_base(decimal, base)
    remainder = decimal
    digits   = []
    until remainder.eql? 0
      digits = [remainder % base] + digits
      remainder /= base
    end
    digits
  end

  def self.validate_base(base)
    raise ArgumentError, 'Base value must be 2 or above' if base < 2
  end

  def self.validate_digits(digits, input_base)
    digits.each do |digit|
      raise ArgumentError, 'Digit value should be bigger than zero' if digit < 0
      raise ArgumentError, 'Digit value should be smaller than its base' if digit >= input_base
    end
  end
end

module BookKeeping
  VERSION = 2
end
