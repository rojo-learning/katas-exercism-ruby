
class Squares
  def initialize(number)
    @number = number
  end

  def difference
    square_of_sums - sum_of_squares
  end

  def sum_of_squares
    @number * (1 + @number) * (2 * @number + 1) / 6
  end

  def square_of_sums
    (@number * (1 + @number) / 2) ** 2
  end
end
