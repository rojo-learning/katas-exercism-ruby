
class SumOfMultiples
  @@bases = [3, 5]

  def self.to(limit, bases = @@bases)
    bases.each_with_object([]) { |base, store|
      (base...limit).step(base) { |multiple| store << multiple }
    }.uniq.reduce(0,:+)
  end

  def initialize(*bases)
    @bases = bases
  end

  def to(limit)
    self.class.to(limit, @bases)
  end
end
