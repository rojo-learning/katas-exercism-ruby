
class School
  YEARS = 1..12

  def initialize
    @db = Hash.new do |grades, year|
      YEARS.include?(year) ? grades[year] = [] : []
    end
  end

  def add(name, year)
    @db[year].push(name).sort!.dup
  end

  def grade(year)
    @db[year].dup
  end

  def to_hash
    Marshal::load Marshal.dump(@db.sort_by { |year| year }.to_h)
  end
end
