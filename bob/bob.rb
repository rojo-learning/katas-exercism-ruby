
class Bob
  def initialize
    @shouting = 'Whoa, chill out!'
    @silence  = 'Fine. Be that way!'
    @question = 'Sure.'
    @other    = 'Whatever.'
  end

  def hey(remark)
    return @shouting if shouting? remark
    return @silence  if silence?  remark
    return @question if question? remark
    @other
  end

  private
  def shouting?(remark)
    remark =~ /[a-z]/i  && remark.upcase.eql?(remark)
  end

  def silence?(remark)
    remark =~ /\A\s*\z/ || remark.empty?
  end

  def question?(remark)
    remark =~ /\?\z/
  end
end
